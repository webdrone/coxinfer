# Coxinfer

Coxinfer holds a python implementation of the inference method for reaction-diffusion process parameters published in:

David Schnoerr, Ramon Grima, and Guido Sanguinetti.
Cox Process Representation and Inference for Stochastic Reaction-Diffusion Processes.
_Nature Communications_ **7** (25 May 2016): 11729.
doi:10.1038/ncomms11729.

The paper regards time point observations of spatially distributed agents as realisations of a Cox process.

This allows one to infer the parameters of the PDEs governing the dynamics of the underlying _intensity fields_, by maximising the likelihood given the observations.


