import scipy as sp
import scipy.optimize as sopt
from scipy.spatial.distance import cdist


# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# % Brownian dynamics simulator
# % simulate a single trajectory of SIRS system system via Brownian
# % dynamics and return positions of particles at discrete time points
# % dt: time discretisation for Brownian simulation
# % drt: interval of measurement time points
# % steps: number of measuremnt time points

def brownian_simulator_SIRS_system(Sini, Iini, Rini, d, inf, diam, rec, sus,
                                   dt, drt, steps):

    S = sp.array(Sini)
    I = sp.array(Iini)
    R = sp.array(Rini)

    i = 1  # %Initialise counter

    ZS = []  # %output
    ZI = []
    ZR = []

    diff = sp.sqrt(2 * d * dt)

    def reflect(a):
        '''if a < 0:
            ref = - (a % -1)
        elif a > 1:
            ref = 1 - (a % 1)
        else:
            ref = a
        '''

        a[a < 0] = - (a[a < 0] % -1)
        a[a > 1] = 1 - (a[a > 1] % 1)
        return a

    while i * dt <= drt * steps:

        if I.size == 0:
            S = Sini
            I = Iini
            R = Rini
            i = 1
            ZS = []
            ZI = []
            ZR = []

        if bool(S.size):
            S = S + (diff * sp.random.randn(*S.shape))
            # S = S + random('Normal', 0, diff, size(S))
        if bool(I.size):
            I = I + (diff * sp.random.randn(*I.shape))
            # I = I + random('Normal', 0, diff, size(I))
        if bool(R.size):
            R = R + (diff * sp.random.randn(*R.shape))
            # R = R + random('Normal', 0, diff, size(R))

        # %Impose reflecting boundary conditions
        S = reflect(S)
        I = reflect(I)
        R = reflect(R)
        # S = arrayfun(@(x) reflect(x), S)
        # I = arrayfun(@(x) reflect(x), I)
        # R = arrayfun(@(x) reflect(x), R)

        if bool(I.size) and bool(S.size):
            '''
            D = squeeze(sqrt(sum(bsxfun(@(a, b) abs(a - b), I, permute(S, [3 2 1])). ^ 2, 2)))
            # %Distance matrix for all particles of type I from type S
            # %if I has only one particle, i.e. only one row, Matlab swaps the indices of D corresponding to I and S. We thus transpose D in that case.
            if length(I(: , 1)) == 1
                D = D'
            '''
            D = cdist(I, S, metric='euclidean')
            cdist
            # % Bimolecular reaction I + S -> I + I
            '''
            row, col = D < diam
            if sum(col) >= 1
                for j = length(col):-1: 1
                    reacted = []
                    if rand < inf * dt
                        if col(j) > length(S(: , 1))
                            'Error1 : col(j)>length(S(:,1))'
                        reacted = [reacted col(j)]
                I = [I; S(reacted, :)];
                S(reacted, :) = [];
                if length(unique(reacted)) < length(reacted)
                    'Error2 : Same S reacted twice'
            '''
            react = D < diam
            # if bool(react_idx.size):
            react_rand = sp.rand(*D.shape)
            react_rand_idx = react_rand < (inf * dt)
            S_react_idx = sp.any(sp.logical_or(react, react_rand_idx), axis=0)
            I = sp.vstack((I, S[S_react_idx]))
            S = sp.delete(S, sp.arange(len(S_react_idx))[S_react_idx], axis=0)

        # % Reation I->R with rate rec (recovery)
        if bool(I.size):
            '''
            if rand < length(I(: , 1))*rec*dt
                ind = randi(length(I(: , 1)));
                R = [R; I(ind, :)];
                I(ind, : ) = [];
            '''
            I_rand = sp.rand(len(I))
            I_idx = I_rand < rec * dt
            R = sp.vstack((R, I[sp.arange(len(I_idx))[I_idx]]))
            I = sp.delete(I, sp.arange(len(I_idx))[I_idx], axis=0)

        # % Reation R->S with rate sus (susceptible)
        if bool(R.size):
            '''
            if rand < length(R(: , 1))*sus*dt
                ind = randi(length(R(: , 1)));
                S = [S; R(ind, :)];
                R(ind, : ) = [];
            '''
            R_rand = sp.rand(len(R))
            R_idx = R_rand < sus * dt
            R = sp.vstack((S, R[sp.arange(len(R_idx))[R_idx]]))
            R = sp.delete(R, sp.arange(len(R_idx))[R_idx], axis=0)

        if (i * dt) % drt == 0:  # % store results in Z's
            ZS += [S]
            ZI += [I]
            ZR += [R]

        i = i + 1

    return ZS, ZI, ZR
