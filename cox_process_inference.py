# % Parameter inference for the SIRS system
# % We consider a system in the [0,1]x[0,1] square with reactions
# % I + S -> I + I  - rate constant: inf , reaction range: diam
# % I -> R  -  rate constant: rec
# % R ->S  -  rate constant: sus
# % other parameters:
# % d: diffusion constant for all species
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# % Executing the function "[logLH,pars]=inference_SIRS_system()" performs
# % parameter inference for one simualted data set by optimising the
# % loglikelihood. The latter is obtained by use of the function
# % "brownian_simulator_SIRS_system()". The optimised value of the
# % loglikelihood and the optimised parameters are returned.

import scipy as sp
import scipy.optimize as sopt
from cox_process_inference_brown_sim import brownian_simulator_SIRS_system


# '''
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# % Compute loglikelihood
def logLikelihood(Sdata, Idata, Rdata, n, d, kmac, rec, sus, ini, drt):

    def evaluateProjSIR(x, y, n, ks):
        xc = int(sp.ceil(x * n))
        yc = int(sp.ceil(y * n))
        n = int(n)
        r = ks[n * (yc - 1) + xc - 1]
        return r

    def mapOnPoints(points, ks):
        ar = 0
        ks_l_sqrt = sp.sqrt(len(ks))
        for p in points:
            ar = ar + sp.log(evaluateProjSIR(p[0], p[1], ks_l_sqrt, ks))
        return ar

    def logMeasLH(points, ks):
        if points.size == 0:
            val = -sum(ks) / len(ks)
        else:
            val = mapOnPoints(points, ks) - sum(ks) / len(ks)
        return val

    def deval(r, x):
        sol = sp.array([r.integrate(x_i) for x_i in x])
        return sol.T

    if d > 0 and kmac > 0 and rec > 0 and sus > 0:
        steps = len(Sdata)
        sol = solvePDE(n, d, kmac, rec, sus, ini, steps * drt)
        x = sp.linspace(drt, steps * drt, steps)
        evs = deval(sol, x)

        Sresp = evs[: n**2, :]  # % for measurements of S
        Iresp = evs[n**2: 2 * n**2, :]  # % for measurements of I
        Rresp = evs[2 * n**2:, :]  # % for measurements of R

        lh = sum([logMeasLH(di, r) for di, r in zip(Sdata, Sresp.T)])
        lh = lh + sum([logMeasLH(di, r) for di, r in zip(Idata, Iresp.T)])
        lh = lh + sum([logMeasLH(di, r) for di, r in zip(Rdata, Rresp.T)])
    else:
        lh = -1e10

    return lh
# '''


# '''
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# %Solve PDE for intensity by basis projection

def solvePDE(n, d, kmac, rec, sus, ini, tf):

    def f_ode(_, c, n, d, k, r, s, lap):
        cS = c[0: n**2]
        cI = c[n**2: 2 * n**2]
        cR = c[2 * n**2: 3 * n**2]

        dcS = d * lap @ cS - k * cS * cI + s * cR
        dcI = d * lap @ cI + k * cS * cI - r * cI
        dcR = d * lap @ cR + r * cI - s * cR
        dk = sp.hstack((dcS.T, dcI.T, dcR.T)).T

        return dk

    def laplaceMat(n):
        diag1 = -1 * sp.hstack((2, 3 * sp.ones(n - 2), 2))
        diag2 = -1 * sp.hstack((3, 4 * sp.ones(n - 2), 3))
        diagonal = sp.hstack((diag1, sp.hstack([diag2] * (n - 2)), diag1))
        offdiag = sp.hstack([0, sp.ones(n - 1)] * n)
        offdiag = offdiag[1:]
        ll = n**2 * (sp.diag(diagonal) +
                     sp.diag(offdiag, 1) +
                     sp.diag(offdiag, -1) +
                     sp.diag(sp.ones(n**2 - n), n) +
                     sp.diag(sp.ones(n**2 - n), -n))

        return ll

    lapl = laplaceMat(n)

    # sol = ode45(@(t,c)ode(n,d,kmac,rec,sus,c,lapl),[0 tf],ini);
    sol = sp.integrate.ode(f_ode).set_integrator('dopri5')
    sol.set_initial_value(ini).set_f_params(n, d, kmac, rec, sus, lapl)

    return sol
# '''


def inference_SIRS_system():
    # rng('shuffle')

    # define system parameters:
    d = 0.001
    inf = 100
    diam = 0.01
    rec = 0.02
    sus = 0.2

    # initial guess for the bimolecular reaction rate of the model
    kmac = inf * diam ** 2 / 2

    # method parameters:
    dt = 1e-4
    drt = 1
    points = 40  # %number of measurement points
    # %number of discretisation intervals in each spatial dimenstion,
    # i.e., n**2 basis functions
    n = 10

    # %initial point configuration:
    SiniTot = 200
    SiniSim = sp.rand(SiniTot, 2)
    IiniSim = sp.array([[1 / (2 * n), 1 / (2 * n)]])
    RiniSim = sp.empty((0, 2))

    # %initial condition of intensity fields:
    SiniProj = SiniTot * sp.ones(n**2)
    IiniProj = n ** 2 * sp.hstack((1, sp.zeros(n ** 2 - 1)))
    RiniProj = sp.zeros(n**2)
    ini = sp.hstack((SiniProj, IiniProj, RiniProj))

    # %simulate data:
    # tic
    print('Simulating data (Brownian simulator)...')
    ZS, ZI, ZR = brownian_simulator_SIRS_system(
        SiniSim, IiniSim, RiniSim, d, inf, diam, rec, sus, dt, drt, points)
    # toc

    # %inference:
    # tic
    print('Performing inference...')
    pars = sp.array([(1.5 * sp.rand() + 0.5) * m for m in (d, kmac, rec, sus)])

    def neg_ll(x):
        nll = -logLikelihood(ZS, ZI, ZR, n, x[0], x[1], x[2], x[3], ini, drt)
        return nll
    optimised_nll = sopt.fmin(func=neg_ll, x0=pars,
                              maxfun=2 * 2000, full_output=True)

    print(optimised_nll)

    pars = optimised_nll[0]
    negLogLH = optimised_nll[1]
    iters = optimised_nll[2]
    # toc

    logLH = -negLogLH
    return logLH, pars, iters


if __name__ == '__main__':
    logLH, pars, iters = inference_SIRS_system()

    print()
    print('Inference converged.')
    print('Number of iterations:', iters)
    print('Log-likelihood:', logLH)
    print('Inferred parameters:', pars)
